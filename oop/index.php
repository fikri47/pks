<?php

require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$sheep = new Animal("shaun");

echo "Name: " . $sheep->name . "<br>";
echo "Legs: " .$sheep->legs . "<br>";
echo "Cold Blooded: " .$sheep->cold_blooded . "<br>";
echo "<br>";

$kodok = new Frog("Kidi");
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " .$kodok->legs . "<br>";
echo "Cold Blooded: " .$kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->Jump() . "<br>";
echo "<br>";


$ape = new Ape("kera sakti");
echo "Name: " . $ape->name . "<br>";
echo "Legs: " .$ape->legs . "<br>";
echo "Cold Blooded: " .$ape->cold_blooded . "<br>";
echo "Yell : " . $ape->Yell();

?>
